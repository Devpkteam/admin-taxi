import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import {Router} from '@angular/router';
import { CorekService } from '../services/corek.service';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    public form:FormBuilder,
    private _corek:CorekService,
    private _auth:AuthenticationService,
    private toastr: ToastrService,
    private router: Router
  ){

    this.loginForm = form.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required],
    });

  }

  ngOnInit() {
  }

  async presentToast(icon, message, color, duration){
    await this.toastr.info('<span class="now-ui-icons '+icon+'"></span> '+message, '', {
      timeOut: duration,
      closeButton: true,
      enableHtml: true,
      toastClass: "alert alert-"+color+" alert-with-icon",
      positionClass: 'toast-bottom-center'
    });
  }

  login(){
    console.log('aqui');
    this.presentToast('loader_refresh spin', 'Espere por favor', 'danger', 2000);
    let conection = Date.now().toString+'conection'+Math.random();
    this._corek.ConnectCorekconfig(conection);
    this._corek.socket.on(conection, (response)=>{
      console.log(response);
      let getLogin = Date.now()+'getLogin'+Math.random()
      this._corek.socket.emit('signon' ,{log:this.loginForm.value.email, pwd:this.loginForm.value.password,'event':getLogin});
      this._corek.socket.on(getLogin, (signon)=>{
        console.log(signon);
        if(signon.token){
          if(signon.user_status == -1){
            this.presentToast('ui-1_lock-circle-open', 'Bienvenido a YouLand Administrador', 'info', 3000);
            this._auth.login(signon.ID);
          }else{
            this.presentToast('business_badge', 'No tienes estatus de administrador', 'danger', 3000);
          }
        }else{
          this.presentToast('business_badge', 'Los datos no concuerdan', 'danger', 3000);
        }
      })
    });
  }
  

}
