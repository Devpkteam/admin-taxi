import { Component, OnInit } from '@angular/core';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Inicio',  icon: 'design_app', class: '' },
    { path: '/create-trip', title: 'Viajes',  icon:'location_map-big', class: '' },
    { path: '/trips', title: 'Ver viaje',  icon:'location_map-big', class: '' },
    { path: '/taxi', title: 'Ver taxi viajes',  icon:'location_map-big', class: '' },
    { path: '/users', title: 'Usuarios',  icon: 'users_single-02', class: '' },
    { path: '/drivers', title: 'Choferes',  icon:'users_single-02', class: '' },
    { path: '/drivers-maps', title: 'Mapa conductores',  icon:'business_globe', class: '' },
    { path: '/payments', title: 'Pagos',  icon:'users_single-02', class: '' },
];

@Component({
  selector: 'app-sidebar', 
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ( window.innerWidth > 991) {
          return false;
      }
      return true;
  };
}
