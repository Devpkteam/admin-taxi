import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator, MatDialog} from '@angular/material';
import { CorekService } from '../../services/corek.service';
import { UsersDetailsComponent } from '../dialogs/users-details/users-details.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  public lineBigDashboardChartType;
  public lineBigDashboardChartData:Array<any>;
  public lineBigDashboardChartLabels:Array<any>;
  public lineBigDashboardChartColors:Array<any>;
  public lineBigDashboardChartOptions:any;
  public gradientStroke;
  public chartColor;
  public canvas : any;
  public ctx;
  public gradientFill;
  public qtyUsers = 0;
  public showBar = true;

  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }

  public hexToRGB(hex, alpha) {
    var r = parseInt(hex.slice(1, 3), 16),
      g = parseInt(hex.slice(3, 5), 16),
      b = parseInt(hex.slice(5, 7), 16);

    if (alpha) {
      return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
    } else {
      return "rgb(" + r + ", " + g + ", " + b + ")";
    }
  }

  displayedColumns: string[] = ['position', 'name', 'rol', 'status', 'options'];
  //dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSource: any;
  users_element = [];

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private _corek:CorekService,
    public dialog: MatDialog
  ){

  }

  ngOnInit() {
    this.makeChart();
    let conection = Date.now().toString+'conection'+Math.random();
    this._corek.ConnectCorekconfig(conection);
    this._corek.socket.on(conection, (response)=>{
      this.getUsers();
    });
  }

  getUsers(){
    this.users_element = [];
    let getUser = Date.now().toString() +'get_user'+Math.random();
      this._corek.socket.emit('query',{'event':getUser, 'querystring':"SELECT * FROM wp_users WHERE user_status = 3 OR user_status = 4"});
      this._corek.socket.on(getUser, (users)=>{
        this.qtyUsers = users.length;
        if(users.length>0){
          users.forEach((user, index) => {
            let rol = 'Pasajero';
            this.users_element.push({id:user.ID, position:index+1, name:user.display_name, rol:rol, status: user.user_status, email:user.user_email, photo:user.user_photo, phone: user.user_phone});
          });
          this.dataSource = new MatTableDataSource(this.users_element);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        }
        this.showBar = false;
      });
  }

  makeChart(){

    this.lineBigDashboardChartType = 'line';

    this.chartColor = "#FFFFFF";
    this.canvas = document.getElementById("bigDashboardChart");
    this.ctx = this.canvas.getContext("2d");

    this.gradientStroke = this.ctx.createLinearGradient(500, 0, 100, 0);
    this.gradientStroke.addColorStop(0, '#80b6f4');
    this.gradientStroke.addColorStop(1, this.chartColor);

    this.gradientFill = this.ctx.createLinearGradient(0, 200, 0, 50);
    this.gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    this.gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

    this.lineBigDashboardChartData = [
      {
        label: "Data",

        pointBorderWidth: 1,
        pointHoverRadius: 7,
        pointHoverBorderWidth: 2,
        pointRadius: 5,
        fill: true,

        borderWidth: 2,
        data: [6,0,0,0,0,0,0,0,0,0,0,0]
      }
    ];
    this.lineBigDashboardChartColors = [
      {
        backgroundColor: this.gradientFill,
        borderColor: this.chartColor,
        pointBorderColor: this.chartColor,
        pointBackgroundColor: "#2c2c2c",
        pointHoverBackgroundColor: "#2c2c2c",
        pointHoverBorderColor: this.chartColor,
      }
    ];
    this.lineBigDashboardChartLabels = ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"];
    this.lineBigDashboardChartOptions = {

          layout: {
              padding: {
                  left: 20,
                  right: 20,
                  top: 0,
                  bottom: 0
              }
          },
          maintainAspectRatio: false,
          tooltips: {
            backgroundColor: '#fff',
            titleFontColor: '#333',
            bodyFontColor: '#666',
            bodySpacing: 4,
            xPadding: 12,
            mode: "nearest",
            intersect: 0,
            position: "nearest"
          },
          legend: {
              position: "bottom",
              fillStyle: "#FFF",
              display: false
          },
          scales: {
              yAxes: [{
                  ticks: {
                      fontColor: "rgba(255,255,255,0.4)",
                      fontStyle: "bold",
                      beginAtZero: true,
                      maxTicksLimit: 5,
                      padding: 10
                  },
                  gridLines: {
                      drawTicks: true,
                      drawBorder: false,
                      display: true,
                      color: "rgba(255,255,255,0.1)",
                      zeroLineColor: "transparent"
                  }

              }],
              xAxes: [{
                  gridLines: {
                      zeroLineColor: "transparent",
                      display: false,

                  },
                  ticks: {
                      padding: 10,
                      fontColor: "rgba(255,255,255,0.4)",
                      fontStyle: "bold"
                  }
              }]
          }
    };

  }

  changeStatus(ID, status){
    let newStaus;
    switch (status){
      case 3:
        newStaus = 4;
      break;
      case 4:
        newStaus = 3;
      break;
    }
    let updateUser = Date.now()+'updateUser'+Math.random();
    this._corek.socket.emit("update_user",{"set":{"user_status":newStaus},"condition":{"ID":ID}, 'event':updateUser});
    this._corek.socket.on(updateUser, (response)=>{
      this.getUsers();
    });

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(ID): void {

    console.log(ID);
    const dialogRef = this.dialog.open(UsersDetailsComponent, {
      width: '50%',
      data: ID
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
