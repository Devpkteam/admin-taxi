import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { MapsComponent } from '../../maps/maps.component';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { UsersComponent } from '../../users/users.component';
import { MaterialModule } from '../../../material/material.module';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { TripsComponent } from '../../trips/trips.component';
import { DriversComponent } from '../../drivers/drivers.component';
import { AutocompleteComponent } from '../../maps/autocomplete/autocomplete.component';
import { UsersDetailsComponent } from '../../dialogs/users-details/users-details.component';
import { ShowImageComponent } from '../../dialogs/show-image/show-image.component';
import { ProgressBarComponent } from '../../progress-bar/progress-bar.component'
import { TripsDetailsComponent } from '../../dialogs/trips-details/trips-details.component';
import { PaymentsComponent } from '../../payments/payments.component';
import { MapaConductoresComponent } from '../../mapa-conductores/mapa-conductores.component';
import { TaxitripsComponent } from '../../../pages/taxitrips/taxitrips.component';
import {MatIconModule} from '@angular/material/icon';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    NgbModule,
    ToastrModule.forRoot(),
    MaterialModule,
    NgxMaterialTimepickerModule,
    MatIconModule
  ],
  declarations: [
    DashboardComponent,
    MapsComponent,
    UsersComponent,
    TripsComponent, 
    DriversComponent,
    AutocompleteComponent,
    UsersDetailsComponent,
    ShowImageComponent,
    ProgressBarComponent,
    TripsDetailsComponent,
    PaymentsComponent,
    MapaConductoresComponent,
    TaxitripsComponent
  ],
  entryComponents: [UsersDetailsComponent, ShowImageComponent, TripsDetailsComponent]
})

export class AdminLayoutModule {}
