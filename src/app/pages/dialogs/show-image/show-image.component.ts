import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { CorekService } from '../../../services/corek.service';

@Component({
  selector: 'app-show-image',
  templateUrl: './show-image.component.html',
  styleUrls: ['./show-image.component.scss']
})
export class ShowImageComponent implements OnInit {

  urlImage;
  constructor(

    @Inject(MAT_DIALOG_DATA) data,
    public dialogRef: MatDialogRef<ShowImageComponent>,
    public _corek: CorekService
  ) { 
    this.urlImage = this._corek.urlUploads+data;
  }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }

}
